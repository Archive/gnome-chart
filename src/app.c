/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * app.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "app.h"

#include <gnome.h>
#include <libguppi/guppi-group-view-layout.h>
#include <libguppi/guppi-view-interval.h>
#include <libguppi/guppi-root-group-view.h>
#include <libguppi/guppi-root-group-item.h>

#include "chart.h"

struct _AppWindow {
  GnomeApp *app;
  ChartContext *cc;

  GtkWidget *status_bar;
  GtkProgress *progress;
  GtkWidget *about;
};

static void
apologize (const gchar *str)
{
  gchar *msg;
  GtkWidget *w;

  msg = g_strdup_printf ("Sorry, but the '%s' function\nisn't implemented yet.", str);
  w = gnome_ok_dialog (msg);
  gtk_widget_show (w);
  g_free (msg);
}

static void
open_cb (GtkWidget *w, gpointer closure)
{
  apologize ("Open");
}

static void
print_cb (GtkWidget *w, gpointer closure)
{
  apologize ("Print");
}

static void
exit_cb (GtkWidget *w, gpointer closure)
{
  gtk_exit (0);
}

static void
about_cb (GtkWidget *w, gpointer closure)
{
  AppWindow *appwin = closure;
  const gchar *authors[] = { "Jon Trowbridge <trow@gnu.org>", NULL };

  if (appwin->about != NULL) {
    g_assert (GTK_WIDGET_REALIZED (appwin->about));
    gdk_window_show (appwin->about->window);
    gdk_window_raise (appwin->about->window);
  } else {

    appwin->about = gnome_about_new ("GnomeChart",
				     VERSION,
				     "Copyright (C) 2001 The Free Software Foundation",
				     authors,
				     NULL,
				     NULL);

    gtk_signal_connect (GTK_OBJECT (appwin->about),
			"destroy",
			GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			&appwin->about);

    gnome_dialog_set_parent (GNOME_DIALOG (appwin->about), GTK_WINDOW (appwin->app));

    gtk_widget_show (appwin->about);
  }
}

static void
appwin_status_cb (const gchar *str, gpointer closure)
{
  app_window_set_status_message ((AppWindow *) closure, str);
}

static void
appwin_progress_cb (gpointer closure)
{
  app_window_indicate_progress ((AppWindow *) closure);
}

static void
entry_activate_cb (GtkEditable *editable, gpointer closure)
{
  AppWindow *appwin = closure;
  gchar *str;

  str = gtk_editable_get_chars (editable, 0, -1);
  app_window_enable_progress (appwin, TRUE);
  chart_context_set_symbol (appwin->cc, str,
			    appwin_status_cb, appwin,
			    appwin_progress_cb, appwin);
  app_window_enable_progress (appwin, FALSE);
  g_free (str);
}

static GnomeUIInfo file_menu[] = {
  GNOMEUIINFO_MENU_OPEN_ITEM (open_cb, NULL),
  GNOMEUIINFO_MENU_PRINT_ITEM (print_cb, NULL),
  GNOMEUIINFO_MENU_EXIT_ITEM (exit_cb, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {
  GNOMEUIINFO_HELP ("gnome-chart"),
  GNOMEUIINFO_MENU_ABOUT_ITEM (about_cb, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo app_menus[] = {
  GNOMEUIINFO_MENU_FILE_TREE (file_menu),
  GNOMEUIINFO_MENU_HELP_TREE (help_menu),
  GNOMEUIINFO_END
};

AppWindow *
create_app_window (void)
{
  AppWindow *appwin;
  GtkWidget *mi, *entry, *box;

  appwin = g_new0 (AppWindow, 1);

  appwin->app = (GnomeApp *) gnome_app_new (PACKAGE, "GnomeChart");

  gtk_signal_connect (GTK_OBJECT (appwin->app),
		      "delete_event",
		      GTK_SIGNAL_FUNC (gtk_exit),
		      NULL);

  appwin->cc = chart_context_new (appwin_status_cb, appwin);
  gnome_app_set_contents (appwin->app, chart_context_get_widget (appwin->cc));

  appwin->status_bar = gnome_appbar_new (TRUE, TRUE, GNOME_PREFERENCES_NEVER);
  gnome_app_set_statusbar (appwin->app, appwin->status_bar);
  app_window_set_status_message (appwin, "Welcome to GnomeChart " VERSION);

  appwin->progress = gnome_appbar_get_progress (GNOME_APPBAR (appwin->status_bar));
  gtk_progress_set_activity_mode (appwin->progress, TRUE);
  gtk_widget_hide (GTK_WIDGET (appwin->progress));

  gnome_app_create_menus_with_data (appwin->app, app_menus, appwin);

  mi = gtk_menu_item_new ();
  entry = gtk_entry_new_with_max_length (8);
  box = gtk_hbox_new (FALSE, 2);
  gtk_box_pack_start (GTK_BOX (box), gtk_label_new ("Symbol:"), FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (box), entry, FALSE, FALSE, 0);

  gtk_container_add (GTK_CONTAINER (mi), box);
  gtk_widget_show_all (mi);

  gtk_menu_item_right_justify (GTK_MENU_ITEM (mi));
  gtk_menu_bar_append (GTK_MENU_BAR (appwin->app->menubar), mi);

  gtk_signal_connect (GTK_OBJECT (entry),
		      "activate",
		      GTK_SIGNAL_FUNC (entry_activate_cb),
		      appwin);

  gtk_widget_show (GTK_WIDGET (appwin->app));
  return appwin;
}

void
app_window_set_status_message (AppWindow *appwin, const gchar *str)
{
  g_return_if_fail (appwin != NULL);
  gnome_appbar_set_status (GNOME_APPBAR (appwin->status_bar), str);
  while (gtk_events_pending ())
    gtk_main_iteration ();
}

void
app_window_enable_progress (AppWindow *appwin, gboolean x)
{
  g_return_if_fail (appwin != NULL);

  if (x)
    gtk_widget_show (GTK_WIDGET (appwin->progress));
  else
    gtk_widget_hide (GTK_WIDGET (appwin->progress));
}

void
app_window_indicate_progress (AppWindow *appwin)
{
  static double x = 0;
  g_return_if_fail (appwin != NULL);
  gtk_progress_set_value (appwin->progress, x);
  x += 0.1;
  if (x > 0.99)
    x = 0;
  while (gtk_events_pending ())
    gtk_main_iteration ();
}
