/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * chart.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "chart.h"

#include <math.h>
#include <gnome.h>
#include <libgnomeprint/gnome-font.h>
#include <libguppi/guppi-group-view-layout.h>
#include <libguppi/guppi-view-interval.h>
#include <libguppi/guppi-root-group-view.h>
#include <libguppi/guppi-root-group-item.h>

#include "indicators.h"
#include "quotes.h"

typedef GuppiData *(*IndicatorDataConstructor) (GuppiPriceSeries *, gpointer closure);
typedef struct _Indicator Indicator;
struct _Indicator {
  gchar *name;
  gint max_width;
  guint32 color;

  IndicatorDataConstructor ts_constructor;
  gpointer ts_closure;
  GtkDestroyNotify ts_closure_destroy_fn;

  GuppiData *ts;
  GuppiElementView *view;
};

struct _ChartContext {
  int refs;
  
  StatusFn status_fn;
  gpointer status_closure;

  gchar *symbol;
  GuppiPriceSeries *data;

  GuppiGroupView *group;

  GuppiElementView *x_ax_view;
  GuppiElementView *y_ax_view;
  GuppiElementView *bg_view;
  GuppiElementView *grid_view;
  GuppiElementView *bars_view;
  GuppiElementView *symbol_text_view;

  GList *indicators;

  GtkWidget *widget;
  GtkWidget *scrollbar;
  GtkAdjustment *adj;

  guint adj_block : 1;
};

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
add_indicator (ChartContext *cc,
	       const gchar *name, guint32 color,
	       IndicatorDataConstructor construct_fn, gpointer closure, GtkDestroyNotify closure_destroy_fn)
{
  Indicator *ind = g_new0 (Indicator, 1);

  ind->name                  = g_strdup (name);
  ind->max_width             = -1;
  ind->color                 = color;
  ind->ts_constructor        = construct_fn;
  ind->ts_closure            = closure;
  ind->ts_closure_destroy_fn = closure_destroy_fn;

  cc->indicators = g_list_prepend (cc->indicators, ind);
}

static GuppiData *
movavg_cb (GuppiPriceSeries *ser, gpointer closure)
{
  return moving_average (ser, GPOINTER_TO_INT (closure));
}

static void
add_moving_average (ChartContext *cc, gint days, guint32 color)
{
  gchar *s = g_strdup_printf ("%d-day moving average", days);
  add_indicator (cc, s, color, movavg_cb, GINT_TO_POINTER (days), NULL);
  g_free (s);
}

static void
set_indicator_data (ChartContext *cc, GuppiPriceSeries *ser)
{
  GList *iter;
  for (iter = cc->indicators; iter != NULL; iter = g_list_next (iter)) {
    Indicator *ind = iter->data;

    if (ind->ts)
      gtk_object_unref (GTK_OBJECT (ind->ts));

    ind->ts = ind->ts_constructor (ser, ind->ts_closure);

    if (ind->view == NULL) {
      GuppiElementState *s = guppi_element_state_new ("linegraph",
						      "color", ind->color,
						      "width", 0.75, /* = 72/96 */
						      NULL);
      ind->view = guppi_element_view_new (s, NULL);
      gtk_object_unref (GTK_OBJECT (s));

      guppi_group_view_layout_same_place (cc->group, ind->view, cc->bars_view);
      guppi_element_view_connect_view_intervals (cc->bars_view, GUPPI_X_AXIS, ind->view, GUPPI_X_AXIS);
      guppi_element_view_connect_view_intervals (cc->bars_view, GUPPI_Y_AXIS, ind->view, GUPPI_Y_AXIS);
    }

    guppi_element_state_set (guppi_element_view_state (ind->view),
			     "ts_data", ind->ts,
			     NULL);
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
handle_scroll_bar_visibility (ChartContext *cc)
{
  GuppiViewInterval *vi = guppi_element_view_axis_view_interval (cc->bars_view, GUPPI_X_AXIS);
  if (fabs (vi->t1 - vi->max) < 1e-4 && fabs (vi->t0 - vi->min) < 1e-4)
    gtk_widget_hide (cc->scrollbar);
  else
    gtk_widget_show (cc->scrollbar);
}

static void
adj_value_changed_cb (GtkAdjustment *adj, gpointer closure)
{
  ChartContext *cc = closure;
  GuppiViewInterval *vi;

  if (cc->adj_block)
    return;

  vi = guppi_element_view_axis_view_interval (cc->bars_view, GUPPI_X_AXIS);
  guppi_view_interval_set (vi, adj->value, adj->value+adj->page_size);
}

static gchar *
describe_number_of_days (gint days_total)
{
  gint years = days_total / 365;
  gint days = days_total % 365;

  if (years == 0) {
    return g_strdup_printf ("%d day%s", days, days > 1 ? "s" : "");
  }

  if (days == 0)
    return g_strdup_printf ("%d year%s", years, years > 1 ? "s" : "");

  return g_strdup_printf ("%d year%s, %d day%s",
			  years, years > 1 ? "s" : "",
			  days, days > 1 ? "s" : "");
}

static void
view_interval_changed_cb (GuppiViewInterval *vi, gpointer closure)
{
  ChartContext *cc = closure;

  cc->adj->value = vi->t0;
  cc->adj->page_size = vi->t1 - vi->t0;

  handle_scroll_bar_visibility (cc);

  cc->adj_block = TRUE;
  gtk_adjustment_changed (cc->adj);
  cc->adj_block = FALSE;

  if (cc->status_fn) {
    GDate dt1, dt2;
    gchar dt1buf[32], dt2buf[32], msg[128];
    gchar *retstat = NULL;
    gchar *numofdays;

    g_date_set_julian (&dt1, (gint) ceil (vi->t0));
    g_date_set_julian (&dt2, (gint) floor (vi->t1));

    g_date_strftime (dt1buf, 32, "%x", &dt1);
    g_date_strftime (dt2buf, 32, "%x", &dt2);

    if (cc->data) {
      double p1, p2, ret;
      gint days;

      while ((guppi_price_series_valid (cc->data, &dt1) & PRICE_OPEN) == 0) {
	g_date_add_days (&dt1, 1);
      }
      while ((guppi_price_series_valid (cc->data, &dt2) & PRICE_CLOSE) == 0) {
	g_date_subtract_days (&dt2, 1);
      }

      p1 = guppi_price_series_open (cc->data, &dt1);
      p2 = guppi_price_series_close (cc->data, &dt2);
      days = g_date_julian (&dt2) - g_date_julian (&dt1);
      numofdays = describe_number_of_days (days);

      if (days <= 365) {
	ret = 100 * (p2 - p1)/p1;
	retstat = g_strdup_printf ("Returned %s%.1f%% over %s.", ret >= 0 ? "+" : "", ret, numofdays);
      } else {
	ret = 100 * (pow (p2 / p1, 365 / (double) days) - 1);
	retstat = g_strdup_printf ("Returned %s%.1f%% (annualized) over %s.", ret >= 0 ? "+" : "", ret, numofdays);
      }
      g_free (numofdays);
    }

    g_snprintf (msg, 128, "Viewing %s to %s.  %s", dt1buf, dt2buf, retstat ? retstat : "");
    g_free (retstat);

    cc->status_fn (msg, cc->status_closure);
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

ChartContext *
chart_context_new (StatusFn status_fn, gpointer status_closure)
{
  ChartContext *cc = g_new0 (ChartContext, 1);
  GuppiViewInterval *vi;
  GuppiRootGroupView *rgv;
  GuppiElementState *state;
  GnomeFont *font;

  const double margin = 7.2;

  cc->refs = 1;
  cc->status_fn = status_fn;
  cc->status_closure = status_closure;

  rgv = guppi_root_group_view_new ();

  cc->group = (GuppiGroupView *) rgv;

  /* x-axis */
  state = guppi_element_state_new ("axis",
				   "position", GUPPI_SOUTH,
				   NULL);
  cc->x_ax_view = guppi_element_view_new (state, NULL);
  gtk_object_unref (GTK_OBJECT (state));

  /* y-axis */
  state = guppi_element_state_new ("axis",
				   "position", GUPPI_WEST,
				   "show_major_labels", FALSE,
				   "show_minor_labels", FALSE,
				   "show_micro_labels", FALSE,
				   NULL);
  cc->y_ax_view = guppi_element_view_new (state, NULL);
  gtk_object_unref (GTK_OBJECT (state));

  /* background */
  state = guppi_element_state_new ("background",
				   "color", 0xe0e0ffff,
				   "color_final", 0xc0c0ffff,
				   NULL);
  cc->bg_view = guppi_element_view_new (state, NULL);
  gtk_object_unref (GTK_OBJECT (state));

  /* grid */
  state = guppi_element_state_new ("frame",
				   NULL);
  cc->grid_view = guppi_element_view_new (state, NULL);
  gtk_object_unref (GTK_OBJECT (state));

  /* price bars */
  state = guppi_element_state_new ("pricebars",
				   NULL);
  cc->bars_view = guppi_element_view_new (state, NULL);
  gtk_object_unref (GTK_OBJECT (state));

  /* symbol text */
  font = gnome_font_new_closest ("Helvetica", GNOME_FONT_HEAVIEST, TRUE, 21);
  state = guppi_element_state_new ("text",
				   "text", " ",
				   "font", font,
				   "gradient", TRUE,
				   "color", 0xff0000ff,
				   "color_final", 0x804080ff,
				   NULL);
  gtk_object_unref (GTK_OBJECT (font));
  cc->symbol_text_view = guppi_element_view_new (state, NULL);
  gtk_object_unref (GTK_OBJECT (state));


  /*** Do Layout ***/

  guppi_group_view_layout_flush_left (cc->group, cc->y_ax_view, margin);
  guppi_group_view_layout_flush_top (cc->group, cc->y_ax_view, margin);

  guppi_group_view_layout_flush_bottom (cc->group, cc->x_ax_view, margin);
  guppi_group_view_layout_flush_right (cc->group, cc->x_ax_view, margin);

  guppi_group_view_layout_horizontally_aligned (cc->group, cc->y_ax_view, cc->bg_view, 0);
  guppi_group_view_layout_vertically_aligned (cc->group, cc->bg_view, cc->x_ax_view, 0);

  guppi_group_view_layout_same_place (cc->group, cc->bg_view, cc->grid_view);
  guppi_group_view_layout_same_place (cc->group, cc->bars_view, cc->grid_view);

  guppi_group_view_layout_same_top (cc->group, cc->bars_view, cc->symbol_text_view);
  guppi_group_view_layout_same_right (cc->group, cc->bars_view, cc->symbol_text_view);
  guppi_group_view_raise (cc->group, cc->bars_view, cc->symbol_text_view);


  /*** Make Connections ***/

  guppi_element_view_set_axis_marker_type (cc->bars_view, GUPPI_X_AXIS, GUPPI_AXIS_DATE);
  guppi_element_view_set_axis_marker_type (cc->bars_view, GUPPI_Y_AXIS, GUPPI_AXIS_SCALAR_LOG10);

  guppi_element_view_connect_axis_markers (cc->bars_view, GUPPI_X_AXIS, cc->grid_view, GUPPI_X_AXIS);
  guppi_element_view_connect_axis_markers (cc->bars_view, GUPPI_Y_AXIS, cc->grid_view, GUPPI_Y_AXIS);
  guppi_element_view_connect_axis_markers (cc->grid_view, GUPPI_X_AXIS, cc->x_ax_view, GUPPI_X_AXIS);
  guppi_element_view_connect_axis_markers (cc->grid_view, GUPPI_Y_AXIS, cc->y_ax_view, GUPPI_Y_AXIS);


  vi = guppi_element_view_axis_view_interval (cc->bars_view, GUPPI_Y_AXIS);
  guppi_view_interval_scale_logarithmically (vi, 10);

  /*** Set Up Adjustment ***/

  cc->adj = (GtkAdjustment *) gtk_adjustment_new (0.5, 0, 1, 0.1, 0.2, 0.2);
  gtk_signal_connect (GTK_OBJECT (cc->adj),
		      "value_changed",
		      GTK_SIGNAL_FUNC (adj_value_changed_cb),
		      cc);

  gtk_signal_connect (GTK_OBJECT (guppi_element_view_axis_view_interval (cc->bars_view, GUPPI_X_AXIS)),
		      "changed",
		      GTK_SIGNAL_FUNC (view_interval_changed_cb),
		      cc);

  cc->scrollbar = gtk_hscrollbar_new (cc->adj);

  /*** Add some default indicators ***/

  add_moving_average (cc, 10, 0xff000080);
  add_moving_average (cc, 30, 0x30300080);

  return cc;
}

void
chart_context_ref (ChartContext *cc)
{
  g_return_if_fail (cc != NULL);
  g_return_if_fail (cc->refs > 0);
  ++cc->refs;
}

void
chart_context_unref (ChartContext *cc)
{
  g_return_if_fail (cc != NULL);
  g_return_if_fail (cc->refs > 0);
  --cc->refs;

  if (cc->refs > 0)
    return;

  g_free (cc->symbol);
  gtk_object_unref (GTK_OBJECT (cc->data));

  gtk_object_unref (GTK_OBJECT (cc->group));

  gtk_object_unref (GTK_OBJECT (cc->x_ax_view));
  gtk_object_unref (GTK_OBJECT (cc->y_ax_view));
  gtk_object_unref (GTK_OBJECT (cc->bg_view));
  gtk_object_unref (GTK_OBJECT (cc->grid_view));
  gtk_object_unref (GTK_OBJECT (cc->bars_view));

  g_free (cc);
}

GtkWidget *
chart_context_get_widget (ChartContext *cc)
{
  g_return_val_if_fail (cc != NULL, NULL);

  if (cc->widget == NULL) {
    GnomeCanvas *canv;
    GuppiRootGroupItem *rgi;
    GtkWidget *extra_box;

    canv = guppi_root_group_view_make_canvas (GUPPI_ROOT_GROUP_VIEW (cc->group),
					      (GuppiCanvasItem **) &rgi);
    guppi_root_group_item_set_resize_semantics (rgi, ROOT_GROUP_RESIZE_FILL_SPACE);

    gtk_widget_set_usize (GTK_WIDGET (canv), 400, 200);

    cc->widget = gtk_vbox_new (FALSE, 0);
    extra_box = gtk_hbox_new (FALSE, 0);
    gtk_container_add (GTK_CONTAINER (extra_box), GTK_WIDGET (canv));
    gtk_box_pack_start (GTK_BOX (cc->widget), extra_box, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX (cc->widget), cc->scrollbar, FALSE, TRUE, 0);

    gtk_widget_show (GTK_WIDGET (canv));
    gtk_widget_show (extra_box);
    gtk_widget_show (cc->widget);
  }

  return cc->widget;
}

void
chart_context_set_symbol (ChartContext *cc, const gchar *symbol,
			  StatusFn status_fn, gpointer status_closure,
			  ProgressFn progress_fn, gpointer progress_closure)
{
  GuppiPriceSeries *ser;
  GuppiViewInterval *vi;
  double va, vb;
  gint sd, ed;

  g_return_if_fail (cc != NULL);

  if ((cc->symbol && symbol && !g_strcasecmp (cc->symbol, symbol)) || (cc->symbol == symbol))
    return;

  guppi_element_state_set (guppi_element_view_state (cc->y_ax_view),
			   "show_major_labels", FALSE,
			   "show_minor_labels", FALSE,
			   "show_micro_labels", FALSE,
			   NULL);

  ser = fetch_quotes (symbol, status_fn, status_closure, progress_fn, progress_closure);

  if (ser == NULL)
    return;

  g_free (cc->symbol);
  cc->symbol = g_strdup (symbol);
  g_strup (cc->symbol);

  guppi_element_state_set (guppi_element_view_state (cc->bars_view),
			   "data", ser,
			   NULL);

  guppi_element_state_set (guppi_element_view_state (cc->symbol_text_view),
			   "text", cc->symbol,
			   NULL);

  guppi_element_state_set (guppi_element_view_state (cc->y_ax_view),
			   "show_major_labels", TRUE,
			   "show_minor_labels", TRUE,
			   "show_micro_labels", TRUE,
			   NULL);

  set_indicator_data (cc, ser);

  if (cc->data)
    gtk_object_unref (GTK_OBJECT (cc->data));
  cc->data = ser;

  guppi_element_view_force_preferred_view (cc->bars_view, GUPPI_Y_AXIS, TRUE);

  vi = guppi_element_view_axis_view_interval (cc->bars_view, GUPPI_X_AXIS);

  sd = g_date_julian ((GDate *) guppi_date_indexed_start (GUPPI_DATE_INDEXED (ser)));
  ed = g_date_julian ((GDate *) guppi_date_indexed_end (GUPPI_DATE_INDEXED (ser)));
  guppi_view_interval_set_bounds (vi, sd - 2, ed + 2);
  guppi_view_interval_set_min_width (vi, 21);

  guppi_element_view_set_preferred_view (cc->bars_view, GUPPI_X_AXIS);
  guppi_view_interval_range (vi, &va, &vb);

  cc->adj->lower = vi->min;
  cc->adj->upper = vi->max;
  cc->adj->value = va;
  cc->adj->step_increment = 7;
  cc->adj->page_increment = 30;
  cc->adj->page_size = vb-va;

  handle_scroll_bar_visibility (cc);

  cc->adj_block = TRUE;
  gtk_adjustment_changed (cc->adj);
  cc->adj_block = FALSE;
}
