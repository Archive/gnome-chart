/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * quotes.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "quotes.h"

#include <libgnome/gnome-util.h>
#include <ctype.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "yahoo.h"

#define CACHE_DIR ".gnome-chart"
#define DIR_MODE 0700

static const char *
cache_dir (void)
{
  static char *cache = NULL;

  if (cache == NULL) {
    cache = g_concat_dir_and_file (g_get_home_dir (), CACHE_DIR);
    mkdir (cache, DIR_MODE);
  }

  return cache;
}

static char *
cache_filename (const char *symbol)
{
  char *filename;
  char *path;

  filename = g_strdup_printf ("%s.data", symbol);
  g_strdown (filename);

  path = g_concat_dir_and_file (cache_dir (), filename);
  g_free (filename);

  return path;
}

static void
save_to_cache (GuppiPriceSeries *ser, const char *symbol)
{
  GuppiXMLDocument *doc;
  char *filename;
  xmlNodePtr xml;

  g_return_if_fail (GUPPI_IS_PRICE_SERIES (ser));
  g_return_if_fail (symbol && *symbol);

  filename = cache_filename (symbol);

  doc = guppi_xml_document_new ();
  xml = guppi_data_export_xml (GUPPI_DATA (ser), doc);
  guppi_xml_document_set_root (doc, xml);
  guppi_xml_document_write_file (doc, filename);
  guppi_xml_document_free (doc);

  g_free (filename);
}

static GuppiPriceSeries *
load_from_cache (const char *symbol)
{
  GuppiXMLDocument *doc;
  char *filename;
  GuppiPriceSeries *ser = NULL;

  g_return_val_if_fail (symbol && *symbol, NULL);

  filename = cache_filename (symbol);
  doc = guppi_xml_document_read_file (filename);
  if (doc != NULL) {
    GuppiData *d = guppi_data_import_xml (doc, guppi_xml_document_get_root (doc));
    if (GUPPI_IS_PRICE_SERIES (d)) {
      ser = (GuppiPriceSeries *) d;
    } else {
      g_message ("import from %s failed", filename);
    }
  }

  g_free (filename);
  guppi_xml_document_free (doc);

  return ser;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static gboolean
needed_dates (GuppiPriceSeries *ser, GDate *sd, GDate *ed)
{
  time_t now;
  struct tm *tm;
  GDate today;
  GDate *ps_sd;
  GDate *ps_ed;

  now = time (NULL);
  tm = localtime (&now);
  g_date_set_dmy (&today, tm->tm_mday, tm->tm_mon+1, 1900+tm->tm_year);

  /* adjust today if it is a weekend */
  if (g_date_weekday (&today) == G_DATE_SATURDAY) {
    g_date_subtract_days (&today, 1);
  } else if (g_date_weekday (&today) == G_DATE_SUNDAY) {
    g_date_subtract_days (&today, 2);
  }

  ps_sd = (GDate *)guppi_date_indexed_start (GUPPI_DATE_INDEXED (ser)); 
  ps_ed = (GDate *)guppi_date_indexed_end (GUPPI_DATE_INDEXED (ser));

  if (!g_date_valid (ps_sd)
      || !g_date_valid (ps_ed) 
      || guppi_date_indexed_size (GUPPI_DATE_INDEXED (ser)) == 0) {
    g_date_set_dmy (sd, 1, 1, 1960); /* Based on Yahoo!'s earliest data */
    *ed = today;
    return TRUE;
  }

  if (g_date_compare (ps_ed, &today) < 0) {
    *sd = *ps_ed;
    g_date_add_days (sd, 1);
    *ed = today;

    if (g_date_compare (sd, ed) > 0)
      return FALSE;

    return TRUE;
  }

  return FALSE;
}


GuppiPriceSeries *
fetch_quotes (const char *symbol,
	      StatusFn status_fn, gpointer status_closure,
	      ProgressFn progress_fn, gpointer progress_closure)
{
  GuppiPriceSeries *ser;
  gint sz;
  GDate start_date, end_date;
  gchar *symup;
  gboolean new_series = FALSE;

  g_return_val_if_fail (symbol && *symbol, NULL);

  symup = g_strdup (symbol);
  g_strup (symup);

  if (status_fn) {
    gchar *msg = g_strdup_printf ("Checking cache for %s...", symup);
    status_fn (msg, status_closure);
    g_free (msg);
  }

  ser = load_from_cache (symbol);
  if (ser == NULL) {
    ser = (GuppiPriceSeries*) guppi_data_new ("GuppiPriceSeries::core");
    g_return_val_if_fail (ser != NULL, NULL);
    new_series = TRUE;
    guppi_data_set_label (GUPPI_DATA (ser), symbol);
  }

  sz = guppi_date_indexed_size (GUPPI_DATE_INDEXED (ser));

  if (needed_dates (ser, &start_date, &end_date)) {
    gboolean get_retval;

    if (status_fn) {
      gchar *msg;

      if (g_date_compare (&start_date, &end_date)) {
	msg = g_strdup_printf ("Requesting data from %d-%d-%d to %d-%d-%d...",
			       g_date_year (&start_date),
			       g_date_month (&start_date),
			       g_date_day (&start_date),
			       g_date_year (&end_date),
			       g_date_month (&end_date),
			       g_date_day (&end_date));
      } else {
	msg = g_strdup_printf ("Requesting data from %d-%d-%d...",
			       g_date_year (&start_date),
			       g_date_month (&start_date),
			       g_date_day (&start_date));
      }

      status_fn (msg, status_closure);
      g_free (msg);
    }

    get_retval = fetch_from_yahoo (symbol, ser, &start_date, &end_date, 
				   status_fn, status_closure,
				   progress_fn, progress_closure);

    if (get_retval) {

      if (guppi_date_indexed_size (GUPPI_DATE_INDEXED (ser)) != sz)
	save_to_cache (ser, symbol);

      if (status_fn) {
	status_fn ("", status_closure);
      }

    } else {

      if (status_fn) {
	gchar *msg;
	if (new_series) {
	  msg = g_strdup_printf ("Can't retrieve %s data.", symup);
	  gtk_object_unref (GTK_OBJECT (ser));
	  ser = NULL;
	} else
	  msg = g_strdup_printf ("Can't retrieve up-to-date %s data; using cache.", symup);

	status_fn (msg, status_closure);
	g_free (msg);
      }

    }
  }
   
  return ser;
}
