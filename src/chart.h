/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * chart.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __CHART_H__
#define __CHART_H__

#include <gtk/gtkwidget.h>
#include "types.h"

struct _ChartContext;
typedef struct _ChartContext ChartContext;

ChartContext *chart_context_new   (StatusFn status_fn, gpointer status_closure);
void          chart_context_ref   (ChartContext *);
void          chart_context_unref (ChartContext *);

GtkWidget    *chart_context_get_widget (ChartContext *);
void          chart_context_set_symbol (ChartContext *, const char *symbol,
					StatusFn status_fn, gpointer status_closure,
					ProgressFn progress_fn, gpointer progress_closure);


#endif /* __CHART_H__ */

