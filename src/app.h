/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * app.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __APP_H__
#define __APP_H__

#include <glib.h>

struct _AppWindow;
typedef struct _AppWindow AppWindow;

AppWindow *create_app_window (void);

void app_window_set_status_message (AppWindow *, const char *);

void app_window_enable_progress (AppWindow *, gboolean);
void app_window_indicate_progress (AppWindow *);


#endif /* __APP_H__ */

