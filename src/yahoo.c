/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * yahoo.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "yahoo.h"

#include <stdio.h>
#include <stdlib.h>
#include "http.h"

#define BUFSIZE 1024

gboolean
fetch_from_yahoo (const char *symbol, 
		  GuppiPriceSeries *ser, 
		  GDate *start_date, GDate *end_date,
		  StatusFn status_fn, gpointer status_closure,
		  ProgressFn progress_fn, gpointer progress_closure)
{
  char *uri;
  int fd, count=0;
  FILE *in;
  char buffer[BUFSIZE];

  g_return_val_if_fail (symbol && *symbol, FALSE);
  g_return_val_if_fail (GUPPI_IS_PRICE_SERIES (ser), FALSE);
  g_return_val_if_fail (start_date && g_date_valid (start_date), FALSE);
  g_return_val_if_fail (end_date && g_date_valid (end_date), FALSE);

  uri = g_strdup_printf ("/table.csv?s=%s&a=%d&b=%d&c=%d&d=%d&e=%d&f=%d&g=d&q=q&y=0",
			 symbol,
			 (gint) g_date_month (start_date),
			 (gint) g_date_day (start_date),
			 (gint) g_date_year (start_date),
			 (gint) g_date_month (end_date),
			 (gint) g_date_day (end_date),
			 (gint) g_date_year (end_date));

  fd = connect_via_http ("chart.yahoo.com", 80, uri, FALSE, 
			 status_fn, status_closure, progress_fn, progress_closure);
  g_free (uri);

  if (fd < 0)
    return FALSE;

  in = fdopen (fd, "r");
  g_return_val_if_fail (in != NULL, FALSE);

  while (fgets (buffer, BUFSIZE, in)) {
    gchar *s = buffer;
    gchar *t = buffer;
    gboolean done = FALSE;
    GDate dt;
    double num[5];
    gint i = -1;

    while (!done) {
      while (*t != ',' && *t != '\0')
        ++t;
      
      if (*t == '\0')
	done = TRUE;
      else 
	*t = '\0';
      
      if (i == -1) {
	g_date_set_parse (&dt, s);
	if (!g_date_valid (&dt)) {
	  done = TRUE;
	}
      } else {
	g_assert (i < 5);
	num[i] = atof (s);
      }
      
      if (!done) {
	++t;
	s = t;
      }
      
      ++i;
    }
    
    if (i == 5) {
      guppi_price_series_set (ser, PRICE_OPEN, &dt, num[0]);
      guppi_price_series_set (ser, PRICE_HIGH, &dt, num[1]);
      guppi_price_series_set (ser, PRICE_LOW, &dt, num[2]);
      guppi_price_series_set (ser, PRICE_CLOSE, &dt, num[3]);
      guppi_price_series_set (ser, PRICE_VOLUME, &dt, num[4]);

      if (count % 10 == 0) {
	if (progress_fn)
	  progress_fn (progress_closure);

	if (status_fn) {
	  gchar buf[64];
	  g_date_strftime (buf, 64, "Processing data from %x", &dt);
	  status_fn (buf, status_closure);
	}
      }
      
      ++count;
    } else {
      
      if (g_date_valid (&dt))
	g_message ("Incomplete record for %d-%d-%d",
		   g_date_year (&dt), g_date_month (&dt), g_date_day (&dt));
    }
    
  }

  return TRUE;
}
