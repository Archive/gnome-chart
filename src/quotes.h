/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * quotes.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __QUOTES_H__
#define __QUOTES_H__

#include <libguppi/guppi-price-series.h>
#include "types.h"

GuppiPriceSeries *fetch_quotes (const char *symbol, 
				StatusFn status_fn, gpointer status_closure,
				ProgressFn progress_fn, gpointer progress_closure);

#endif /* __QUOTES_H__ */

