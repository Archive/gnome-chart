/* $Id$ */

/*
 * http.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "http.h"

#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>

#include <gtk/gtkmain.h>

ssize_t 
good_read (gint fd, gpointer ptr, gsize N)
{
  gsize remaining = N;
  while (remaining > 0) {
    /* This should have a time-out */
    ssize_t q = read (fd, ptr, remaining);
    if (q == -1)
      return -1;
    if (q == 0)
      break;
    ptr = ((gchar*)ptr) + q;
    remaining -= q;
  }
  return N - remaining;
}

ssize_t 
good_write (gint fd, gconstpointer ptr, gsize N)
{
  gsize remaining = N;
  
  while (remaining > 0) {
    ssize_t q = write (fd, ptr, remaining);
    if (q == -1)
      return -1;
    if (q == 0)
      break;
    ptr = ((gchar*)ptr) + q;
    remaining -= q;
  }
  return N - remaining;
}

#define BUFFER_LEN 256
gint
connect_via_http (const gchar* host,
		  gint port,
		  const gchar* uri,
		  const gchar** headers,
		  StatusFn status_fn, gpointer status_closure,
		  ProgressFn progress_fn, gpointer progress_closure)
{
  gint sock=-1, fd=-1, fd2=-1;
  struct hostent* hostdata;
  struct sockaddr_in addr;
  gint len, i, rv, total_bytes;
  gchar* req = NULL;
  gchar* host_header = NULL;
  gchar buffer[BUFFER_LEN] = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
  float http_version;
  gint response_code;
  char* tmp_filename = NULL;
  gboolean found_content = FALSE;
  gboolean success = FALSE;

  g_return_val_if_fail (host != NULL, -1);
  g_return_val_if_fail (port > 0, -1);
  g_return_val_if_fail (uri != NULL, -1);

  /* Create a socket, connect to the server */

  sock = socket (AF_INET, SOCK_STREAM, 0);
  g_return_val_if_fail (sock != -1, -1);

  hostdata = gethostbyname (host);
  if (hostdata == NULL) 
    goto cleanup;

  memset (&addr, 0, sizeof (addr));
  addr.sin_family = hostdata->h_addrtype;
  memcpy ((char*)&addr.sin_addr, hostdata->h_addr, hostdata->h_length);
  addr.sin_port = htons (80);

  if (status_fn) {
    gchar *str = g_strdup_printf ("Connecting to %s:%d", host, port);
    status_fn (str, status_closure);
    g_free (str);
  }

  rv = connect (sock, (struct sockaddr*)&addr, sizeof (addr));
  if (rv < 0) 
    goto cleanup;

  req = g_strdup_printf ("GET %s HTTP/1.1\r\n", uri);
  host_header = g_strdup_printf ("Host: %s\r\n", host);
  
  rv = good_write (sock, req, len = strlen(req));
  if (rv != len)
    goto cleanup;

  rv = good_write (sock, host_header, len = strlen(host_header));
  if (rv != len)
    goto cleanup;

  if (headers != NULL) {
    i = 0;
    while (headers[i] != NULL) {
      rv = good_write (sock, headers[i], len = strlen(headers[i]));
      if (rv != len)
        goto cleanup;
      if (good_write (sock, "\r\n", 2) != 2)
        goto cleanup;
    }
  }

  if (good_write (sock, "\r\n", 2) != 2)
    goto cleanup;

  /* Now check the http header of the response. */

  rv = good_read (sock, buffer, 17);
  if (rv != 17)
    goto cleanup;

  rv = sscanf (buffer, "HTTP/%f %d OK", &http_version, &response_code);
  if (rv != 2)
    goto cleanup;
  if (response_code != 200)
    goto cleanup;

  /* 
     Start pulling in the results, looking for the end of the header. 
     When we find that point, start dumping stuff into a temporary file.

     We actually create 2 fds for the temporary file: the one we use to
     write it, and a second one (creatively name fd2) which is read-only,
     and which is the one that we actually return.
  */

  tmp_filename = g_strdup ("/tmp/gnomechartXXXXXX");
  fd = mkstemp(tmp_filename);
  if (fd == -1)
    goto cleanup;

  fd2 = open (tmp_filename, O_RDONLY);
  if (fd2 == -1)
    goto cleanup;

  /* This is a great UNIX trick: if you delete an open file, it will
     stay around until all file handles referring to it are closed,
     or until the app terminates.  By doing this, you never have to
     worry about leaving stray files laying around... */
  unlink (tmp_filename);

  total_bytes = 0;
  while ((rv = good_read (sock, buffer, BUFFER_LEN)) > 0) {

    total_bytes += rv;

    if (progress_fn)
      progress_fn (progress_closure);

    if (status_fn) {
      gchar *msg = g_strdup_printf ("Read %d bytes...", total_bytes);
      status_fn (msg, status_closure);
      g_free (msg);
    }
      
    if (found_content) {
      good_write (fd, buffer, rv);
    } else {
      /* Look for the end of the headers */
      gint i;
      for (i=0; i<rv-2 && !found_content; ++i)
        if ((buffer[i] == '\n' && buffer[i+1] == '\n') ||
            (i < rv-4 && buffer[i] == '\r' && buffer[i+1] == '\n' &&
             buffer[i+2] == '\r' && buffer[i+3] == '\n')) {
          found_content = TRUE;
          good_write (fd, buffer+i+2, rv-(i+2));
        } 
    }
  }

  if (!found_content)
    goto cleanup;
  
  success = TRUE;

  cleanup:
  g_free (req);
  g_free (host_header);
  g_free (tmp_filename);

  if (sock >= 0)
    close (sock);
  if (fd >= 0)
    close (fd);
    
  if (success)
    return fd2;
  else {
    close (fd2);
    return -1;
  }
}

/* $Id$ */
