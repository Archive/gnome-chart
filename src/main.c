/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * main.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <gnome.h>

#include <libguppi/guppi-debug.h>
#include <libguppi/guppi-useful-init.h>
#include <libguppi/guppi-data-init.h>
#include <libguppi/guppi-plot-init.h>
#include <libguppi/guppi-plug-in-spec.h>

#include "quotes.h"
#include "app.h"

int
main (int argc, char *argv[])
{
  bindtextdomain (PACKAGE, GNOMELOCALEDIR);
  textdomain (PACKAGE);

  gnome_init (PACKAGE, VERSION, argc, argv);

  /* All of these init functions are pretty annoying. */
  guppi_useful_init_without_guile ();
  guppi_data_init ();
  guppi_plot_init ();

  /* It is also annoying that we need to call this explicitly. */
  guppi_plug_in_spec_find_all ();

  //guppi_set_verbosity (GUPPI_VERBOSE);

  create_app_window ();

  gtk_main ();

  return 0;
}

