/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * indicators.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "indicators.h"

#include <libguppi/guppi-date-series.h>

typedef struct _MovAvg MovAvg;
struct _MovAvg {
  GuppiPriceSeries *ser;
  gint days;
  double *buf;
};

static MovAvg *
movavg_new (GuppiPriceSeries *ser, gint days)
{
  MovAvg *movavg = g_new (MovAvg, 1);

  movavg->ser = ser;
  movavg->days = days;
  movavg->buf = g_new (double, days);

  gtk_object_ref (GTK_OBJECT (ser));
  return movavg;
}

static void
movavg_free (gpointer ptr)
{
  MovAvg *movavg = ptr;
  gtk_object_unref (GTK_OBJECT (movavg->ser));
  g_free (movavg->buf);
  g_free (movavg);
}

static void
movavg_bounds (GDate *sd, GDate *ed, gpointer closure)
{
  MovAvg *movavg = closure;

  if (sd) {
    *sd = *guppi_date_indexed_start (GUPPI_DATE_INDEXED (movavg->ser));
    if (g_date_valid (sd))
      guppi_date_indexed_step (GUPPI_DATE_INDEXED (movavg->ser), 
			       sd, movavg->days-1);
  }

  if (ed)
    *ed = *guppi_date_indexed_end (GUPPI_DATE_INDEXED (movavg->ser));
}

static gboolean
movavg_valid (GDate *dt, gpointer closure)
{
  MovAvg *movavg = closure;

  return guppi_date_indexed_valid (GUPPI_DATE_INDEXED (movavg->ser), dt);
}

static double
movavg_get (GDate *dt, gpointer closure)
{
  MovAvg *movavg = closure;
  gint count, i;
  double sum = 0;

  count = guppi_price_series_get_many (movavg->ser, dt, -movavg->days,
				       NULL, NULL, NULL, NULL, movavg->buf);

  for (i = 0; i < count; ++i)
    sum += movavg->buf[i];

  return sum / count;
}

static gint
movavg_get_range (GDate *sd, GDate *ed,
		  double *in_tbuf, double *in_buf,
		  gint bufsize, gpointer closure)
{
  MovAvg *movavg = closure;
  GDate sd2 = *sd;
  gint i, j, N;
  double *tbuf, *buf;
  double sum = 0;

  guppi_date_indexed_step (GUPPI_DATE_INDEXED (movavg->ser), &sd2, - movavg->days);
  N = (gint) g_date_julian (ed) - (gint) g_date_julian (&sd2) + 1;

  tbuf = g_new (double, N);
  buf  = g_new (double, N);

  N = guppi_price_series_get_range (movavg->ser, &sd2, ed, tbuf, NULL, NULL, NULL, buf);

  if (N < movavg->days) {
    g_free (tbuf);
    g_free (buf);
    return 0;
  }

  for (i = 0; i < movavg->days-1; ++i)
    sum += buf[i];

  j = 0;
  while (i < N && j < bufsize) {
    sum += buf[i];

    in_tbuf[j] = tbuf[i];
    in_buf[j]  = sum / movavg->days;
    ++i;
    ++j;

    sum -= buf[i - movavg->days];
  }

  g_free (tbuf);
  g_free (buf);

  return j;
}

GuppiData *
moving_average (GuppiPriceSeries *ser, gint days)
{
  GuppiData *d;

  g_return_val_if_fail (GUPPI_IS_PRICE_SERIES (ser), NULL);
  g_return_val_if_fail (days >= 1, NULL);

  d = guppi_data_new ("GuppiDateSeries::calc");
  gtk_object_set (GTK_OBJECT (d),
		  "bounds_fn", movavg_bounds,
		  "valid_fn", movavg_valid,
		  "get_fn", movavg_get,
		  "get_range_fn", movavg_get_range,
		  "user_data", movavg_new (ser, days),
		  "user_data_destroy_fn", movavg_free,
		  NULL);

  return d;
}
