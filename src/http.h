/* $Id$ */

/*
 * http.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_HTTP_H
#define _INC_HTTP_H

/*
  Utility/convenience routines for network access.
*/

#include <glib.h>
#include <unistd.h>
#include "types.h"

ssize_t good_read (gint fd, gpointer ptr, gsize num_bytes);
ssize_t good_write (gint fd, gconstpointer ptr, gsize num_bytes);

gint connect_via_http (const gchar *host, gint port,
		       const gchar *uri, const gchar **http_headers,
		       StatusFn status_fn, gpointer status_closure,
		       ProgressFn progress_fn, gpointer progess_closure);


#endif /* _INC_HTTP_H */

/* $Id$ */

